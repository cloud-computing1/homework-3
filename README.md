# Homework 3

## Testing locally

In order to test locally, do the following:

1. Run `npm install`
1. Add a `.env` file for each service folder and define the variables needed in `src/config/env.ts`
1. Add a `key.json` file in each service folder
1. Run `npm run debug`

You should be able to run the services locally

## Deploying

For the `manager` service, run:

```bash
$ gcloud app deploy
```

For the `destroyer` service, run:

```bash
$ gcloud functions deploy cleanup_function --runtime=nodejs14 --trigger-topic="topic" --region="region" --entry-point=run --source .
```
