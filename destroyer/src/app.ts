import { initDatabase, initRepositories, initServices } from './init'
import { FilesService } from './services'

export async function app () {
  initDatabase()
  initRepositories()
  initServices()

  await FilesService.getInstance().clear()
}
