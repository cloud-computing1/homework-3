import dotenv from 'dotenv'
import { tidyEnv } from 'tidyenv'

dotenv.config()

export const env = tidyEnv.process(process.env, {
  CREDENTIAL_FILE_NAME: tidyEnv.str(),
  BUCKET_NAME: tidyEnv.str()
})
