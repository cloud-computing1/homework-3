export class File {
  public id!: string;
  public name!: string;
  public size!: number;
  public createdAt!: Date;
  public expiringAt!: Date;
}
