import { Datastore } from '@google-cloud/datastore'
import { Singleton, SingletonContainer } from '../../types'
import { array, singleton } from '../../utils'
import { File } from '../entities'

export interface Repository extends Singleton {
  getExpiredFiles: (date: Date) => Promise<File[]>;
  removeFiles: (files: File[]) => Promise<void>;
}

const container: SingletonContainer<Repository> = {
  current: null
}

export const getInstance = (databaseClient?: Datastore) =>
  singleton.create<Repository>(container, () => {
    if (!databaseClient) {
      throw new Error('[Files repository] Init error')
    } else {
      return constructor(databaseClient)
    }
  })

function constructor (databaseClient: Datastore): Repository {
  const kind = 'File'

  return {
    getExpiredFiles,
    removeFiles
  }

  async function getExpiredFiles (date: Date): Promise<File[]> {
    const query = databaseClient.createQuery(kind).filter('expiringAt', '<', date)
    const [tasks] = await databaseClient.runQuery(query)

    const files = tasks.map<File>((task) => ({
      id: task.id,
      name: task.name,
      size: task.size,
      createdAt: task.createdAt,
      expiringAt: task.expiringAt
    }))

    return files
  }

  async function removeFiles (files: File[]): Promise<void> {
    const fileKeys = files.map(file => databaseClient.key([kind, file.id]))
    const batches = array(fileKeys).batch(1000)

    await array(batches).mapPromises(batch => databaseClient.delete(batch))
  }
}
