import { app } from './app'

export const run = async () => {
  await app()

  console.info(`[Destroyer] Ran function at ${new Date().toISOString()}`)
}
