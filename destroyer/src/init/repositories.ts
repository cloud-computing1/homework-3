import { DatabaseClient } from '../database'
import { FilesRepository } from '../database/repositories'

export function init () {
  const databaseClient = DatabaseClient.getInstance().get()

  FilesRepository.getInstance(databaseClient)
}
