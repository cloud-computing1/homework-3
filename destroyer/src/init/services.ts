import { env } from '../config'
import { FilesRepository } from '../database/repositories'
import { FilesService, GoogleCloudService, StorageService } from '../services'

export function init () {
  const filesRepository = FilesRepository.getInstance()

  const googleCloudStorage = GoogleCloudService.getInstance()
  const storage = googleCloudStorage.getStorage()
  const bucket = storage.bucket(env.BUCKET_NAME)

  const cloudStorage = StorageService.getInstance(bucket)
  FilesService.getInstance(filesRepository, cloudStorage)
}
