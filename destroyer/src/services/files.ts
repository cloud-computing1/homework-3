import { StorageService } from '.'
import { FilesRepository } from '../database/repositories'
import { Singleton, SingletonContainer } from '../types'
import { array, singleton } from '../utils'

export interface Service extends Singleton {
  clear: () => Promise<void>;
}

const container: SingletonContainer<Service> = {
  current: null
}

export const getInstance = (filesRepository?: FilesRepository.Repository, storageService?: StorageService.Service) =>
  singleton.create(container, () => {
    if (!filesRepository || !storageService) {
      throw new Error('[Files service] Init error')
    } else {
      return constructor(filesRepository, storageService)
    }
  })

function constructor (filesRepository: FilesRepository.Repository, storageService: StorageService.Service): Service {
  return {
    clear
  }

  async function clear (): Promise<void> {
    const expiryDate = new Date()
    const files = await filesRepository.getExpiredFiles(expiryDate)

    await array(files).mapPromises(async (file) => {
      try {
        await storageService.remove(file.id)

        console.info(`[Files service] File with id ${file.id} was deleted successfully`)
      } catch (err) {
        if (err.code === 404) {
          console.info(`[Files service] File with id ${file.id} was already deleted`)
        } else {
          console.error('[Files service]', err.message, '\n', err.stack)
        }
      }
    })

    await filesRepository.removeFiles(files)

    console.info(`[Files service] Removed ${files.length} files`)
  }
}
