import { Datastore } from '@google-cloud/datastore'
import { Storage } from '@google-cloud/storage'
import { Singleton, SingletonContainer } from '../types'
import { googleCloud, singleton } from '../utils'

export interface Service extends Singleton {
  getStorage: () => Storage;
  getDatastore: () => Datastore;
}

const container: SingletonContainer<Service> = {
  current: null
}

export const getInstance = () => singleton.create<Service>(container, constructor)

function constructor (): Service {
  const credentialsPath = googleCloud.credentialsPath()
  const storage = new Storage({ keyFilename: credentialsPath })
  const datastore = new Datastore({ keyFilename: credentialsPath })

  return {
    getStorage,
    getDatastore
  }

  function getStorage (): Storage {
    return storage
  }

  function getDatastore (): Datastore {
    return datastore
  }
}
