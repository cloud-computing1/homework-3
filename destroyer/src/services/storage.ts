import { Bucket } from '@google-cloud/storage'
import { Singleton, SingletonContainer } from '../types'
import { singleton } from '../utils'

export interface Service extends Singleton {
  remove: (id: string) => Promise<void>;
}

const container: SingletonContainer<Service> = {
  current: null
}

export const getInstance = (bucket?: Bucket) =>
  singleton.create(container, () => {
    if (!bucket) {
      throw new Error('[Storage service] Init error')
    } else {
      return constructor(bucket)
    }
  })

function constructor (bucket: Bucket): Service {
  return {
    remove
  }

  async function remove (id: string): Promise<void> {
    await bucket.file(id).delete()
  }
}
