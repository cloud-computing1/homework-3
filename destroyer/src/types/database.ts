import { Datastore } from '@google-cloud/datastore'

export type DBClient = {
  current: undefined | Datastore;
};
