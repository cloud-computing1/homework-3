type MapPromises<T> = <R>(callback: (currentValue: T, index?: number, array?: T[]) => Promise<R>) => Promise<R[]>;
type Batch<T> = (count: number) => T[][];

interface ArrayFunctions<T> {
  mapPromises: MapPromises<T>;
  batch: Batch<T>;
}

export const array = <T>(instance: T[]): ArrayFunctions<T> => ({
  mapPromises: mapPromisesCurry<T>(instance),
  batch: batchCurry<T>(instance)
})

const mapPromisesCurry = <T>(instance: T[]): MapPromises<T> => (callback) => {
  const promises = instance.map(callback)
  return Promise.all(promises)
}

const batchCurry = <T>(instance: T[]): Batch<T> => (count) => {
  const batches = instance.reduce((accumulator, value, index) => {
    if (index % count === 0) {
      accumulator.push([value])
    } else {
      accumulator[accumulator.length - 1]?.push(value)
    }

    return accumulator
  }, [] as T[][])
  return batches
}
