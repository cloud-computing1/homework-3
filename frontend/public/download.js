if (window.location.pathname !== '/') {
  timedDownload()
  defaultText()
}

function timedDownload () {
  setTimeout(download, 2000)
}

async function download () {
  const file = window.location.pathname.slice(1)

  try {
    await downloadFile(file)
  } catch (err) {
    errorText(err)
    timer = setTimeout(() => defaultText(), 5000)
  }
}

async function downloadFile (file) {
  const response = await getSignedReadUrl(file)
  await browserDownloadHack(response.uploadUrl, file)
}

async function getSignedReadUrl (file) {
  const request = await fetch(`${backendUri}/files/${file}`, {
    method: 'GET'
  })

  const response = await request.json()

  return response
}

async function browserDownloadHack (signedUrl, file) {
  const a = document.createElement('a')
  a.style.display = 'none'
  a.href = signedUrl
  a.download = file

  document.body.appendChild(a)
  a.click()
}

function defaultText () {
  const title = `Download for file ${window.location.pathname.slice(1)}.`
  const paragraph = 'Download will begin in 2 seconds...'

  presenter.children[0].innerHTML = title
  presenter.children[1].innerHTML = paragraph
}

function errorText (err) {
  const title = 'Could not download file'
  const paragraph = `Finished with error: ${err.message}`

  presenter.children[0].innerHTML = title
  presenter.children[1].innerHTML = paragraph
}
