if (window.location.pathname === '/') {
  document.body.addEventListener('dragover', dragover, false)
  document.body.addEventListener('drop', drop, false)
  defaultText()
}

function dragover (event) {
  event.preventDefault()
}

async function drop (event) {
  event.preventDefault()
  clearTimeout(timer)
  defaultText()

  const dataTransfer = event.dataTransfer
  const files = dataTransfer.files

  if (files.length > 1) {
    errorText()
    timer = setTimeout(() => defaultText(), 5000)
  } else {
    try {
      const [response] = await Promise.all([...files].map(async (file) => upload(file)))

      successMessage(response.id)
    } catch (err) {
      errorText(err)
      timer = setTimeout(() => defaultText(), 5000)
    }
  }
}

async function upload (file) {
  const response = await getSignedWriteUrl(file)
  await uploadToGoogleCloud(response.uploadUrl, file)

  return response
}

async function getSignedWriteUrl (file) {
  const payload = {
    name: file.name,
    size: file.size,
    contentType: file.type
  }

  const request = await fetch(`${backendUri}/files`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(payload)
  })

  const response = await request.json()

  return response
}

async function uploadToGoogleCloud (signedUrl, file) {
  await fetch(signedUrl, {
    method: 'PUT',
    headers: { 'Content-Type': file.type },
    body: file
  })
}

function defaultText () {
  const title = 'Welcome.'
  const paragraph = 'Drag something in this window to upload...'

  presenter.children[0].innerHTML = title
  presenter.children[1].innerHTML = paragraph
}

function errorText (err) {
  const title = 'Could not upload file(s)'
  const paragraph = err ? `Finished with error: ${err.message}` : 'Try to upload only 1 file'

  presenter.children[0].innerHTML = title
  presenter.children[1].innerHTML = paragraph
}

function successMessage (id) {
  const title = 'Successfully uploaded file'
  const paragraph = `It can be found at address: ${window.location.protocol}//${window.location.host}/${id}`

  presenter.children[0].innerHTML = title
  presenter.children[1].innerHTML = paragraph
}
