import express from 'express'
import { initControllers, initRouters, initServer, initServices } from './init'

export async function app () {
  const server = express()

  initServices()
  initControllers()
  initRouters()
  initServer(server)

  return server
}
