import dotenv from 'dotenv'
import { tidyEnv } from 'tidyenv'

dotenv.config()

export const env = tidyEnv.process(process.env, {
  PORT: tidyEnv.num({ default: 3000 }),
  BACKEND_URI: tidyEnv.str()
})
