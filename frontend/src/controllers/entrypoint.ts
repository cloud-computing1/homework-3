import express from 'express'
import { EntrypointService } from '../services'
import { Singleton, SingletonContainer } from '../types'
import { singleton } from '../utils'

export interface Controller extends Singleton {
  index: (req: express.Request, res: express.Response) => Promise<void>;
}

const container: SingletonContainer<Controller> = { current: null }

export const getInstance = (entrypointService?: EntrypointService.Service) =>
  singleton.create<Controller>(container, () => {
    if (!entrypointService) {
      throw new Error('[Entrypoint controller] Init error')
    } else {
      return constructor(entrypointService)
    }
  })

function constructor (entrypointService: EntrypointService.Service): Controller {
  return {
    index
  }

  async function index (_: express.Request, res: express.Response) {
    res.render('index', entrypointService.index())
  }
}
