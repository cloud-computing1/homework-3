import { EntrypointController } from '../controllers'
import { EntrypointService } from '../services'

export function init () {
  const entrypointService = EntrypointService.getInstance()

  EntrypointController.getInstance(entrypointService)
}
