import { EntrypointController } from '../controllers'
import { EntrypointRouter } from '../routers'

export function init () {
  const entrypointController = EntrypointController.getInstance()

  EntrypointRouter.getInstance(entrypointController)
}
