import express from 'express'
import path from 'path'
import { EntrypointRouter } from '../routers'

export function init (app: express.Application): void {
  const entrypointRouter = EntrypointRouter.getInstance().get()

  app.use(express.static(path.join(__dirname, '../..', 'public')))
  app.set('view engine', 'pug')

  app.use(entrypointRouter)
}
