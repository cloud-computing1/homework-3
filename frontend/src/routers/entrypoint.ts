import express from 'express'
import { EntrypointController } from '../controllers'
import { Singleton, SingletonContainer } from '../types'
import { routing, singleton } from '../utils'

export interface Router extends Singleton {
  get: () => express.Router;
}

const container: SingletonContainer<Router> = {
  current: null
}

export const getInstance = (entrypointController?: EntrypointController.Controller) =>
  singleton.create<Router>(container, () => {
    if (!entrypointController) {
      throw new Error('[Entrypoint router] Init error')
    } else {
      return constructor(entrypointController)
    }
  })

function constructor (entrypointController: EntrypointController.Controller): Router {
  const router = express.Router()

  setup(router)

  return {
    get
  }

  function setup (router: express.Router): void {
    const upgradedRouter = routing.asyncRouter(router)

    upgradedRouter.get('/', entrypointController.index)
    upgradedRouter.get('/:fileId', entrypointController.index)
  }

  function get (): express.Router {
    return router
  }
}
