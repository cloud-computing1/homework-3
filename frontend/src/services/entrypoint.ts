import { env } from 'config'
import { Singleton, SingletonContainer } from '../types'
import { singleton } from '../utils'

export interface Service extends Singleton {
  index: () => Record<string, string>;
}

const container: SingletonContainer<Service> = {
  current: null
}

export const getInstance = () => singleton.create(container, constructor)

function constructor (): Service {
  return {
    index
  }

  function index () {
    return {
      BACKEND_URI: env.BACKEND_URI
    }
  }
}
