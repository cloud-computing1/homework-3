import express from 'express'
import { initControllers, initDatabase, initRepositories, initRouters, initServer, initServices } from './init'

export async function app () {
  const server = express()

  initDatabase()
  initRepositories()
  initServices()
  initControllers()
  initRouters()
  initServer(server)

  return server
}
