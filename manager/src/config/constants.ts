import { HTTPErrorResponse } from '../types'

export const ONE_HOUR = 60 * 60 * 1000
export const GENERIC_500_ERROR: HTTPErrorResponse = { code: 500, message: 'Internal server error' }
