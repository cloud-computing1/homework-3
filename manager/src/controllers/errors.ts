import express from 'express'
import { constants } from '../config'
import { ErrorsService } from '../services'
import { Singleton, SingletonContainer } from '../types'
import { errors, singleton } from '../utils'

export interface Controller extends Singleton {
  handle: (err: Error, req: express.Request, res: express.Response, _: express.NextFunction) => Promise<void>;
}

const container: SingletonContainer<Controller> = { current: null }

export const getInstance = (errorsService?: ErrorsService.Service) =>
  singleton.create<Controller>(container, () => {
    if (!errorsService) {
      throw new Error('[Errors controller] Init error')
    } else {
      return constructor(errorsService)
    }
  })

function constructor (errorsService: ErrorsService.Service): Controller {
  return {
    handle
  }

  async function handle (
    err: Error | errors.HTTPError,
    _1: express.Request,
    res: express.Response,
    _2: express.NextFunction
  ) {
    const isHTTPError = errors.isHTTPError(err)

    const response = isHTTPError ? errorsService.handle(err as errors.HTTPError) : constants.GENERIC_500_ERROR

    if (!isHTTPError) {
      console.error(err)
    }

    res.status(response.code).send({ message: response.message })
  }
}
