import express from 'express'
import { FilesService } from '../services'
import { Singleton, SingletonContainer } from '../types'
import { errors, singleton } from '../utils'

export interface Controller extends Singleton {
  create: (req: express.Request, res: express.Response) => Promise<void>;
  getByFileId: (req: express.Request, res: express.Response) => Promise<void>;
}

const container: SingletonContainer<Controller> = { current: null }

export const getInstance = (filesService?: FilesService.Service) =>
  singleton.create<Controller>(container, () => {
    if (!filesService) {
      throw new Error('[Files controller] Init error')
    } else {
      return constructor(filesService)
    }
  })

function constructor (filesService: FilesService.Service): Controller {
  return {
    create,
    getByFileId
  }

  async function create (req: express.Request, res: express.Response) {
    const name = req?.body?.name
    const size = req?.body?.size
    const contentType = req?.body?.contentType

    if (
      !name ||
      !size ||
      !contentType ||
      typeof name !== 'string' ||
      typeof size !== 'number' ||
      typeof contentType !== 'string'
    ) {
      throw errors.badParameters()
    }

    const file = await filesService.create(name, contentType, size)

    res.status(201).send(file)
  }

  async function getByFileId (req: express.Request, res: express.Response) {
    const fileId = req.params.fileId

    if (!fileId) {
      throw errors.badParameters()
    }

    const file = await filesService.getByFileId(fileId)

    if (!file) {
      throw errors.notFound()
    }

    res.status(200).send(file)
  }
}
