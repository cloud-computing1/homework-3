import { Datastore } from '@google-cloud/datastore'
import { DBClient, Singleton, SingletonContainer } from '../types'
import { singleton } from '../utils'

export interface Client extends Singleton {
  get: () => undefined | Datastore;
  set: (datastore: Datastore) => void;
}

const container: SingletonContainer<Client> = {
  current: null
}

export const getInstance = () => singleton.create(container, constructor)

function constructor (): Client {
  const client: DBClient = { current: undefined }

  return {
    get,
    set
  }

  function get (): undefined | Datastore {
    return client.current
  }

  function set (datastore: Datastore): void {
    client.current = datastore
  }
}
