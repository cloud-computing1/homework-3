import { Datastore } from '@google-cloud/datastore'
import { constants } from '../../config'
import { CryptoService } from '../../services'
import { Singleton, SingletonContainer } from '../../types'
import { singleton } from '../../utils'
import { File } from '../entities'

export interface Repository extends Singleton {
  create: (name: string, size: number) => Promise<File>;
  getByFileId: (fileId: string) => Promise<File | null>;
}

const container: SingletonContainer<Repository> = {
  current: null
}

export const getInstance = (databaseClient?: Datastore, cryptoService?: CryptoService.Service) =>
  singleton.create<Repository>(container, () => {
    if (!databaseClient || !cryptoService) {
      throw new Error('[Files repository] Init error')
    } else {
      return constructor(databaseClient, cryptoService)
    }
  })

function constructor (databaseClient: Datastore, cryptoService: CryptoService.Service): Repository {
  const kind = 'File'

  return {
    create,
    getByFileId
  }

  async function create (name: string, size: number) {
    const file = new File()
    file.id = cryptoService.id()
    file.name = name
    file.size = size
    file.createdAt = new Date()
    file.expiringAt = new Date(new Date(file.createdAt).getTime() + constants.ONE_HOUR)

    const taskKey = databaseClient.key([kind, file.id])
    const task = {
      key: taskKey,
      data: file
    }

    await databaseClient.insert(task)

    return file
  }

  async function getByFileId (fileId: string): Promise<File | null> {
    const taskKey = databaseClient.key([kind, fileId])
    const [file] = await databaseClient.get(taskKey)

    if (!file || new Date(file.expiringAt) < new Date()) {
      return null
    }

    return file
  }
}
