import { app } from './app'
import { env } from './config'

app().then((app) =>
  app.listen(env.PORT, () => {
    console.log('[Server] Up on port', env.PORT)
  })
)
