import { ErrorsController, FilesController } from '../controllers'
import { ErrorsService, FilesService } from '../services'

export function init () {
  const filesService = FilesService.getInstance()
  const errorsService = ErrorsService.getInstance()

  FilesController.getInstance(filesService)
  ErrorsController.getInstance(errorsService)
}
