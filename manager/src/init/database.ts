import { DatabaseClient } from '../database'
import { GoogleCloudService } from '../services'

export function init () {
  const googleCloudStorage = GoogleCloudService.getInstance()
  const datastore = googleCloudStorage.getDatastore()

  DatabaseClient.getInstance().set(datastore)
}
