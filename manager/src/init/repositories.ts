import { DatabaseClient } from '../database'
import { FilesRepository } from '../database/repositories'
import { CryptoService } from '../services'

export function init () {
  const databaseClient = DatabaseClient.getInstance().get()
  const cryptoService = CryptoService.getInstance()

  FilesRepository.getInstance(databaseClient, cryptoService)
}
