import { FilesController } from '../controllers'
import { FilesRouter } from '../routers'

export function init () {
  const filesController = FilesController.getInstance()

  FilesRouter.getInstance(filesController)
}
