import bodyParser from 'body-parser'
import cors from 'cors'
import express from 'express'
import { ErrorsController } from '../controllers'
import { FilesRouter } from '../routers'

export function init (app: express.Application): void {
  const filesRouter = FilesRouter.getInstance().get()
  const errorsController = ErrorsController.getInstance()

  app.use(cors({ methods: '*', origin: '*' }))
  app.use(bodyParser.json())
  app.use(filesRouter)
  app.use(errorsController.handle)
}
