import express from 'express'
import { FilesController } from '../controllers'
import { Singleton, SingletonContainer } from '../types'
import { routing, singleton } from '../utils'

export interface Route extends Singleton {
  get: () => express.Router;
}

const container: SingletonContainer<Route> = {
  current: null
}

export const getInstance = (filesController?: FilesController.Controller) =>
  singleton.create<Route>(container, () => {
    if (!filesController) {
      throw new Error('[File router] Init error')
    } else {
      return constructor(filesController)
    }
  })

function constructor (filesController: FilesController.Controller): Route {
  const router = express.Router()

  setup(router)

  return {
    get
  }

  function setup (router: express.Router): void {
    const upgradedRouter = routing.asyncRouter(router)

    upgradedRouter.post('/files', filesController.create)
    upgradedRouter.get('/files/:fileId', filesController.getByFileId)
  }

  function get (): express.Router {
    return router
  }
}
