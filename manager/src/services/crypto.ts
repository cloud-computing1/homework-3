import * as uuid from 'uuid'
import { Singleton, SingletonContainer } from '../types'
import { singleton } from '../utils'

export interface Service extends Singleton {
  id: () => string;
}

const container: SingletonContainer<Service> = {
  current: null
}

export const getInstance = () => singleton.create(container, constructor)

function constructor (): Service {
  return {
    id
  }

  function id (): string {
    return uuid.v4()
  }
}
