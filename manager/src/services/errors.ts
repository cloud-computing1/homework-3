import { HTTPErrorResponse, Singleton, SingletonContainer } from '../types'
import { errors, singleton } from '../utils'

export interface Service extends Singleton {
  handle: (err: errors.HTTPError) => HTTPErrorResponse;
}

const container: SingletonContainer<Service> = {
  current: null
}

export const getInstance = () => singleton.create(container, constructor)

function constructor (): Service {
  return {
    handle
  }

  function handle (err: errors.HTTPError): HTTPErrorResponse {
    const code = err.code
    const message = err.message

    return {
      code,
      message
    }
  }
}
