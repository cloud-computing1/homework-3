import { FilesRepository } from '../database/repositories'
import { PublicFile, Singleton, SingletonContainer } from '../types'
import { singleton } from '../utils'
import { StorageService } from './'

export interface Service extends Singleton {
  create: (name: string, contentType: string, size: number) => Promise<PublicFile>;
  getByFileId: (fileId: string) => Promise<PublicFile | null>;
}

const container: SingletonContainer<Service> = {
  current: null
}

export const getInstance = (
  filesRepository?: FilesRepository.Repository,
  cloudStorageService?: StorageService.Service
) =>
  singleton.create(container, () => {
    if (!filesRepository || !cloudStorageService) {
      throw new Error('[Files service] Init error')
    } else {
      return constructor(filesRepository, cloudStorageService)
    }
  })

function constructor (
  filesRepository: FilesRepository.Repository,
  cloudStorageService: StorageService.Service
): Service {
  return {
    create,
    getByFileId
  }

  async function create (name: string, contentType: string, size: number): Promise<PublicFile> {
    const file = await filesRepository.create(name, size)
    const url = await cloudStorageService.signWriteUrl(file.id, contentType, new Date(file.expiringAt))

    const mappedFile: PublicFile = { ...file, uploadUrl: url }

    return mappedFile
  }

  async function getByFileId (fileId: string): Promise<PublicFile | null> {
    const file = await filesRepository.getByFileId(fileId)

    if (!file) {
      return null
    }

    const url = await cloudStorageService.signReadUrl(file.id, new Date(file.expiringAt))

    const mappedFile: PublicFile = { ...file, uploadUrl: url }

    return mappedFile
  }
}
