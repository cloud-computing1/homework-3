import { Bucket, GetSignedUrlConfig, GetSignedUrlResponse } from '@google-cloud/storage'
import { Singleton, SingletonContainer } from '../types'
import { singleton } from '../utils'

export interface Service extends Singleton {
  signReadUrl: (file: string, expiringAt: Date) => Promise<string>;
  signWriteUrl: (file: string, contentType: string, expiringAt: Date) => Promise<string>;
}

const container: SingletonContainer<Service> = {
  current: null
}

export const getInstance = (bucket?: Bucket) =>
  singleton.create(container, () => {
    if (!bucket) {
      throw new Error('[CloudStorage service] Init error')
    } else {
      return constructor(bucket)
    }
  })

function constructor (bucket: Bucket): Service {
  return {
    signReadUrl,
    signWriteUrl
  }

  async function signReadUrl (file: string, expiringAt: Date): Promise<string> {
    const config: GetSignedUrlConfig = {
      action: 'read',
      expires: expiringAt
    }

    const signedUrl = await signUrl(file, config)

    return signedUrl[0]
  }

  async function signWriteUrl (file: string, contentType: string, expiringAt: Date): Promise<string> {
    const config: GetSignedUrlConfig = {
      action: 'write',
      contentType: contentType,
      expires: expiringAt
    }

    const signedUrl = await signUrl(file, config)

    return signedUrl[0]
  }

  async function signUrl (file: string, config: GetSignedUrlConfig): Promise<GetSignedUrlResponse> {
    const response = await bucket.file(file).getSignedUrl(config)

    return response
  }
}
