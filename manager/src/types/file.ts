import { File } from '../database/entities'

export type PublicFile = File & {
  uploadUrl: string;
};
