export class HTTPError extends Error {
  code: number;
  message: string;

  constructor (code: number, message: string) {
    super(message)
    this.code = code
    this.message = message
  }
}

export const isHTTPError = (err: Error | HTTPError): err is HTTPError => {
  if (!(err instanceof HTTPError)) {
    return false
  }

  return Object.keys(err).includes('code')
}

export const badParameters = () => new HTTPError(400, 'Bad parameters')
export const notFound = () => new HTTPError(404, 'Entity was not found')
