import path from 'path'
import { env } from '../config'

export function credentialsPath (): string {
  const filePath = path.join(__dirname, '../../', env.CREDENTIAL_FILE_NAME)

  return filePath
}
